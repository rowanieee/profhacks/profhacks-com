# profhacks-com

This repository contains the ProfHacks event website that is used for
paritcipant registration and other event-related information. The live website
is available at [profhacks.com](https://profhacks.com).

# Table of Contents

1. [Development Environment Installation](#Development+Environment+Information)
    1. [Windows](#Windows)
        1. [Windows: WSL Setup](#Windows%3a-WSL-Setup)
        2. [Windows: Standard Setup](#Windows%3a-Standard-Setup)
    2. [macOS](#macOS)
    3. [Linux](#Linux)
    4. [Optional: Setting Up PostgreSQL](#Optional%3a-Setting-Up-PostgreSQL)
2. [Development](#Development)
    1. [Installing required packages](#Installing-required-packages)
    2. [Testing and Viewing Changes](#Testing-and-Viewing-Changes)

# Development Environment Installation

This website can be developed on all major operating systems. This includes
Windows, macOS, and most Linux distributions (Ubuntu, Arch, etc). Below are
the initial steps that you should take to get the programming environment
set up.

The recommended text editor for this project is [VS Code](https://code.visualstudio.com/).

## Windows

There are two methods of working on this project on Windows. You can either use the
standard Windows versions of the different programs, or Windows Subsystem for
Linux (WSL). WSL is a small, lightweight virtual machine that runs a Linux
distribution (free and open source operating system). The most used
distribution for WSL is Ubuntu. From there, all of the software can be installed
with a few commands.

### Windows: WSL Setup

WSL can be used to quickly set up a development environment. To start, head to the
[WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
This guide will provide you with a Powershell command. NOTE: The Powershell command
must be executed as an elevated user. WHEN OPENING POWERSHELL, MAKE SURE TO
_RUN AS ADMINISTRATOR_.

WSL 2 is not currently required for this project, so you can skip from step 2 to
step 6 in the guide. At step 6, feel free to select any distribution. However,
Ubuntu is the preferred distribution that the team uses for development.

After rebooting your system, you should be able to access Ubuntu by pressing
the Windows key and typing _Ubuntu_. From there, you can create your user account
and follow the [Linux](#Linux) setup steps to install the rest of the software.

### Windows: Standard Setup

1. Install [Git Bash](https://git-scm.com/) from the link provided
2. Download or clone the git repository in Git Bash:

    ```
    git clone https://gitlab.com/rowanieee/profhacks/profhacks-com.git
    ```

3. Install [Node.js LTS](https://nodejs.org/en/)
4. (Optional) Install [PostgreSQL](https://www.postgresql.org/download/windows/)

## macOS

For macOS, the preferred way of installing all of the packages is through the
[Homebrew Package Manager](https://brew.sh/). This can be installed by going
to the provided link and entering the command on the page into the _Terminal_
app.

For the standard command line pagackages like Git, you can install them by
installing Xcode.

For installing the rest of the development software, please follow the Homebrew
commands in the [Linux](#Linux) section

## Linux

1. Install _git_ and _curl_ using apt: `sudo apt install git curl`, Xcode (macOS), or Homebrew (macOS):

    ```
    brew install git curl
    ```

2. Download or clone the git repository:

    ```
    git clone https://gitlab.com/rowanieee/profhacks/profhacks-com.git
    ```

3. Install the [Node Version Manager](https://github.com/nvm-sh/nvm) or `nvm` for short via:

    ```
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
    ```

4. Install the LTS version of the Node Package Manager (npm):

    ```
    nvm install --lts
    ```

5. (Optional) Install PostgreSQL using apt:

    ```
    sudo apt install postgresql postgresql-contrib
    ```

    or Homebrew (macOS):

    ```
    brew install postgresql
    ```

## Optional: Setting Up PostgreSQL

NOTE: This section hasn't been updated with the latest commands. Additionally,
the same commands may not work between Linux and macOS.

Setup PostreSQL according to your OS, and make sure the postgres service is running. See OS specific instructions. For Debian based linux distros, you can start postgreSQL by running:

    ```
    sudo etc/init.d/postgres start
    ```

For macOS:

    ```
    pg_ctl -D /usr/local/var/postgres start
    ```

Access PostgreSQL script and type in the following script:

    ```
    sudo -u postgres -i
    psql
    CREATE DATABASE "prof";
    \connect prof;
    CREATE ROLE "hoot" WITH LOGIN SUPERUSER;
    \password hoot;
    ```

Set the password to "hoot".

# Development

## Installing required packages

The website primarily uses NodeJS to manage all of the software packages used by
the website. To install all of the packages, please run the following command
in the website directory:

```
npm install
```

## Testing and Viewing Changes
1. Move to the ProfHacks repository folder if not there already:

    ```
    cd \<insert-path-here\>/profhacks-com
    ```

2. Run the command:

    ```
    npm run dev
    ```

3. You should see no errors appear from the page or your terminal
4. Open a web browser to [localhost:3000](127.0.0.1:3000)

## Database Migrations

To migrate the database, you first need to create a new migration using the
following command:

```
npx knex migrate:make <migration_name>
```

After creating the file, add the new changes needed for the database. Then run
the following command:

```
npx knex migrate:latest --env=production
```

The command pushes the newest migration to the production database. Make sure
you have the proper environment variables loaded for the database to update
properly.

