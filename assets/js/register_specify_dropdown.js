$(document).ready(function() {
  $('#school_specify').hide()
  $('#gender_specify').hide()
  $('#race_specify').hide()
  $('#accepted_student_competing').hide()

  $('#school').change(function() {
    if($(this).val() == 'specify')
      $('#school_specify').show()
    else
      $('#school_specify').hide()
  })

  $('#gender').change(function() {
    if($(this).val() == 'specify')
      $('#gender_specify').show()
    else
      $('#gender_specify').hide()
  })

  $('#race').change(function() {
    if($(this).val() == 'specify')
      $('#race_specify').show()
    else
      $('#race_specify').hide()
  })

  $('#accepted_student').change(function() {
    if($(this).val() == 'specify')
      $('#accepted_student_competing').show()
    else
      $('#accepted_student_competing').hide()
  })

});
