/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
document.addEventListener('DOMContentLoaded', function() {
	var openbtn = document.getElementById('open-button'),
		closebtn = document.getElementById('close-button'),
		isOpen = false;

	function init() {
		openbtn.addEventListener('click', toggleMenu);
		if( closebtn ) {
			closebtn.addEventListener('click', toggleMenu);
		}	
	}

	function toggleMenu() {
		if(isOpen) {
			document.body.classList.remove('show-menu');
		}
		else {
			document.body.classList.add('show-menu');
		}
		isOpen = !isOpen;
	}

	init();

});
