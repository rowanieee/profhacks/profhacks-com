const {env} = require('process')

module.exports = {
  apiKey: env.MAILGUN_API_KEY,
  domain: env.MAILGUN_DOMAIN

}
