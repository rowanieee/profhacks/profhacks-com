
exports.up = function(knex) {
  return knex.schema.createTable('hacker', table => {
    table.increments('id').primary();
    table.string('first_name').notNullable().comment('first name');
    table.string('last_name').notNullable().comment('last name');
    table.string('phone').notNullable().comment('phone number');
    table.string('email').notNullable().comment('email address');
    table.string('school').notNullable().comment('school');
    table.date('birthdate').notNullable().comment('birthdate');
    table.string('gender').comment('gender');
    table.string('race').comment('race');
    table.string('major').comment('what their major is if they are in college');
    table.enu('los', ['Middle School', 'High School', 'College/University']).notNullable().comment('level of study');
    table.integer('graduation').unsigned().notNullable().comment('year of graduation');
    table.string('country').notNullable().comment('country they are from');
    table.string('state').notNullable().comment('state in their respective country');
    table.string('ieee_member_id').comment('ieee member id if applicable');
    table.binary('resume').comment('resume upload');
    table.enu('shirt_size', ['XXS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL']).notNullable().comment('their t-shirt size');
    table.bool('accepted_student').notNullable().comment('are they a Rowan accepted student');
    table.bool('accepted_student_half_full').comment('are they a Rowan accepted student staying for just half-day (true) or the full-day (false)');
    table.text('dietary_restrictions').comment('any dietary restrictions or alleges');
    table.bool('minor').notNullable().comment('are they younger than 18 years old (true)');
    table.bool('first_timer').notNullable().comment('is this their first ProfHacks (true)');
    table.bool('mlh_coc').notNullable().comment('have they agreed to the MLH code of conduct (true)');
    table.bool('mlh_ctac_pp').notNullable().comment('have they agreed to the MLH privacy policy (true)');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('hacker')
};
