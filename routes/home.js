const mailgunfile = require('../mailgunfile')
const mailgun = require('mailgun-js')(mailgunfile);

module.exports = async function(req, res){
  if(req.method === "GET") {
    res.render("home");
  }
  else if(req.method === "POST") {
    try{
      console.log("post in home");
      const post = req.body;
      const name = post.name;
      const email = post.email;
      const message = post.message;

      await emailMessage(name, email, message);
      return res.render("contact_message", {message: "Thanks For Contacting Us!"});
    }
    catch(err){
      console.log(err, err.stack);
      return global.routes.internal_error(req, res);
    }
  }
  else
    return global.routes.page_not_found(req, res);
}

async function emailMessage(name, email, message) {
  const data = {
    from: `${name} ${email}`,
    to: 'contact@mail.profhacks.com; support@mail.profhacks.com',
    subject: `[ProfHacks 2021 Contact Form] ${name} ${email}`,
    text: message
  };

  mailgun.messages().send(data, (error, body) => {
    console.log(body);
  });

}
