module.exports = function(req, res){
  res.status(404);
  res.render("core/page_not_found");
}
