const knexfile = require('../knexfile')

const knex = require('knex')(knexfile.production)

const mailgunfile = require('../mailgunfile')
const mailgun = require('mailgun-js')(mailgunfile);

module.exports = async function(req, res) {

  const {method} = req

  if(method === 'GET'){
    res.render('register')
  }

  if(method === 'POST'){
    console.log(req.body)
    const {first_name, last_name, phone, email, street, city, state, country, zipcode, school, school_specify, birthdate, gender, gender_specify, race, race_specify, major, los, graduation, ieee_member_id, shirt_size, accepted_student, accepted_student_competing, minor, first_timer, mlh_coc, mlh_ctac_pp} = req.body;

    const shipping_address = street + " " + city + ", " + state + ", " + country + " " + zipcode;

    const hacker = {
      first_name
      , last_name
      , phone
      , email
      , shipping_address
      , school: school === 'specify' ? school_specify : school
      , birthdate
      , gender: gender === 'specify' ?  gender_specify : gender
      , race: race === 'specify' ? race_specify : race
      , major
      , los
      , graduation
      , country
      , state
      , ieee_member_id
      , shirt_size
      , accepted_student: accepted_student === 'specify' ? true : false
      , accepted_student_half_full: accepted_student_competing === 'Competing' ? true : false // This variable is called "half-full" but was switched to competing/only workshops for the virtual event
      , minor: minor === undefined ? false: true
      , first_timer: first_timer === undefined ? false: true
      , mlh_coc: mlh_coc === undefined ? false: true
      , mlh_ctac_pp: mlh_ctac_pp === undefined ? false: true
    }

    console.log(hacker)
    try{
      insertHacker(hacker)
      sendHackerEmailConfirmation(hacker)
    }
    catch(err) {
      console.log(err)
      return res.status(400).render('internal_error')
    }
	  return res.render('register', {message: "Registered Successfully!"})

  }
}

function insertHacker(hacker) {
  knex('hacker_2021')
    .insert(hacker)
    .then((data) => {
      return data
    })

}

function sendHackerEmailConfirmation(hacker) {

  const data = {
    from: 'ProfHacks Team team@mail.profhacks.com',
    to: hacker.email,
    subject: 'Profhacks 2021 Registration Confirmation',
    text:
    `
    Hello ${hacker.first_name}!

    You have been registered for ProfHacks 2021! Stay tuned for more information as the event approaches.

    Thank you for registering!
    - ProfHacks Team
    `
  };

  mailgun.messages().send(data, (error, body) => {
    console.log(body);
  });

}
